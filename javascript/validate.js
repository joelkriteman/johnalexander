function validateContForm()
{
	errors = '';
	dateerror = '';
	
	//RegExp
	validemail     = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	validname      = /^([a-zA-Z\-'])+ *([a-zA-Z\- '])*$/;
	validdate      = /^\d{1,2}\D+\d{1,2}\D+\d{2,4}$/;
	validphone     = /^[\d ]{10,}$/;
	validpc1       = /^[a-zA-Z0-9 ]{2,}$/;
	validpc2       = /^[a-zA-Z0-9 ]{3,}$/;
	validlongpc    = /^[a-zA-Z0-9 ]{2,}$/;
	validcurrency  = /^[0-9]{4,}$/;
	valid3digit    = /^[0-9]{3}$/;
	valid4digit    = /^[0-9]{4}$/;
	validnumber    = /^[0-9]{1,}$/;
	valid2digit    = /^[0-9]{2}$/;
	valid8digit    = /^[0-9]{8}$/;


   var oMyForm = document.contactform;
// regular expression patterns
   
// test for basic mandatory form field values
   
	if((oMyForm.name.value.trim().length == 0) || (!validname.test(oMyForm.name.value)))
	{errors+='Provide your name.\n';}

	if((oMyForm.emailaddr.value !='') && (!validemail.test(oMyForm.emailaddr.value)))
	{errors+='Provide a valid email address.\n';}

	if((oMyForm.contactnum.value !='') && (!validphone.test(oMyForm.contactnum.value)))
	{errors+='Contact telephone number is invalid.\n';}
   
	if((oMyForm.emailaddr.value == '') && (oMyForm.contactnum.value == ''))
	{errors+='Provide at least one contact method.\n';}

	if(oMyForm.enquiry.value.trim().length == 0)
	{errors+='Please tell us what your enquiry is.\n';}
		
// show errors or submit form
   if (errors){alert(errors);}
   else{oMyForm.submit();}
}