<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>John Alexander Advisory | Independent Impartial Business Advisor and Insolvency Practitioner</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="x-ua-compatible" content="IE=9" />
<meta name="keywords" content="John Alexander Advisory, John Alexander, Business Advisor, Financial Difficulties, Insolvency Practitioners, Business Advice, Liquidations, Administrations, Bankruptcies, Directors Disqualification Act, Administrators, Pre-Pack" />
<meta name="description" content="John Alexander Advisory is available to assist lawyers and accountants in advising their clients who are in financial difficulties or in their dealings with Insolvency Practitioners." />
<meta name="author" content="Lodge Information Services Limited | www.lisltd.co.uk" />
<meta name="copyright" content="Copyright <?php echo(date('Y'));?> " />
<meta name="robots" content="index, follow" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="revisit-after" content="7 Days" />
<meta name="format-detection" content="telephone=no" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="container">
   <div id="bgwrap">
	<div id="bg">
	 <div id="maincontentwrap">
		<div id="logowrap">
		 <div id="logoleft">
		   <a href="index.php"><img src="images/logo.png" alt="John Alexander logo" border="0" title="John Alexander Advisory logo"></a>
		 </div><!-- end logoleft div -->
		 <div id="logocentre">
		  <h1>John Alexander Advisory</h1>
		  <h2>Independent Impartial Business Advisor</br>and Insolvency Practitioner</h2>		  
		 </div><!-- end logocentre div -->
		 <div id="logoright">
		  <img src="images/JohnAlexander.png" alt="Photo of John Alexander" border="0" title="Photo of John Alexander">
		 </div><!-- end logoright div -->
		</div><!-- end logowrap div -->
		<div id="navhome">
		 <ul>
		  <li><a href="index.php">Home</a></li>
		  <li><a href="activities.php">Activities</a></li>
		  <li><a href="press.php">Press</a></li>
		  <li><a href="contact.php">Contact</a></li>
		 </ul>
		</div><!-- end of nav div -->
		<div id="hpmaincontupper">
		 <p class="mainupper">John Alexander is available to assist lawyers and accountants in advising their clients who are in financial difficulties or in their dealings with Insolvency Practitioners.</p>
		 <p class="mainupper">As an experienced Insolvency Practitioner who has spent over 30 years giving business advice, John Alexander no longer takes on formal insolvency appointments.  He has been a partner in KPMG, PKF and CBW, but is now an independent and impartial business advisor.</p>
		</div><!-- end upper content div -->
		<div id="hpmaincontlower">
		 <div id="hpmaincontlowerleft">
		  <p class="mainlowerhead">Experience</p>
		  <p class="mainlower">John Alexander has led some of the UK's largest and most complex Liquidations and Administrations as well as many very much smaller ones and some of the more interesting personal Bankruptcies.</p>
          <p class="mainlower">He has been Receiver of the Rosehaugh Group, the UK's largest property insolvency at the time.</p>
          <p class="mainlower">He has been Administrator of the Dawnay, Day Group, the Fashion Caf&eacute;, the international Albany Group, as well as Trustee in Bankruptcy of Darius Guppy and the so-called 'Lady Rosemary Aberdour'.</p>
		 </div><!-- end lower left div -->
		 <div id="hpmaincontlowerright">
		  <p class="mainlowerhead">Services Offered</p>
		  <p class="mainlower">John Alexander may assist professional advisors by working with them to explain the practical options available to their clients as well as the practical impact of the advice being offered.</p>
		  <p class="mainlower">His advice can assist in determining whether there is a light at the end of the tunnel and if there is, if it is the way out or a train coming the other way!  He can also assist in their dealings with Insolvency Practitioners, claims under the Company's Directors Disqualification Act, or buying businesses from Administrators, perhaps as part of a Pre-Pack.</p>
		 </div><!-- end lower right div -->
		</div><!-- end lower content div -->
		<div id="hpmaincontact">
		  <p><a href="contact.php">Click here to contact John Alexander, business advisor, to see how we can assist.</a></p>
		</div><!-- end contact div -->
     </div><!-- end maincontentwrap div -->
     <div id="footerwrap">
	  <div id="footerleft">
	   <p>&copy; <?php echo(date('Y'));?> John Alexander (Advisory) Limited. All Rights Reserved</p>
	  </div><!-- end footer left div -->
	  <div id="footerright">
	   <p><a href="http://uk.linkedin.com/pub/john-alexander/4/8a1/235/" target="_blank"><img src="images/Linkedin.png" title="Follow John Alexander on LinkedIn" alt="Follow John Alexander on LinkedIn" border="0"></a>&nbsp;<a href="https://twitter.com/jag_alexander" target="_blank"><img src="images/Twitter.png" title="Follow John Alexander on Twitter" alt="Follow John Alexander on Twitter" border="0"></a></p>
	  </div><!-- end footer right div -->
     </div><!-- end footer wrap div -->  
	</div><!-- end bg div -->
   </div><!-- end bgwrap div -->
   <div id="footerliswrap">
	<div id="footerlis">
	 <p>Designed by <a href="http://www.lisltd.co.uk">Lodge Information Services Ltd</a></p>
	</div><!-- end lis footer content --> 
   </div><!-- end lis footer wrap div -->
</div><!-- end container div -->
</body>
</html>