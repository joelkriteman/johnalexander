<?php
session_start();

//Create array of referrer elements
$referer = parse_url($_SERVER['HTTP_REFERER']);

//Session var set should match name of page
$sessionvar = str_replace('.php', '', str_replace('/', '', $referer['path']));

//Referring host
$host = $referer['host'];

//Permitted hosts
$permhost = array('dev.lisltd.co.uk', 'johnalexanderadvisory.co.uk', 'www.johnalexanderadvisory.co.uk', 'johnalexander.carciofino.co.uk');

//Referring page
$page = str_replace('/', '', $referer['path']);

//Permitted pages with forms
$permpage = array('contact.php','ja_test2contact.php','ja_testcontact.php');

#echo($sessionvar.'<br>');
#echo($host.'<br>');
#echo($page.'<br>');
			  
if((isset($_SESSION[$sessionvar])) && ($_SESSION[$sessionvar] == md5('ja')) && (in_array($host, $permhost)) && (in_array($page, $permpage)))
{
	// Injections to detect
	$injexs = array("http:\/\/","https:\/\/","<a href=","<\/a>");

	// Loop post array and assign to $form with escape char and whitespace trimmed
	while(list($key, $val) = @each($_POST))
	{
		foreach ($injexs as $inj_key => $inj_val)
		{
			if(preg_match('/'.$inj_val.'/i', $val))
			{
				$error++;
			}
		}
			
		$form[$key] = addslashes(trim($val));
		reset($injexs);
	}
	
	// Go away!
	if($error)
	{
		header('Location: /index.php');
		exit();
	}
	
	// Include mailer class
	require("class.phpmailer.php");
	
	// Create new instance
	$mail = new PHPMailer();
	
	// Typical headers
	$mail->From = "website@johnalexanderadvisory.co.uk";
	$mail->FromName = "JAA Website";
	$mail->Sender = "website@johnalexanderadvisory.co.uk";
	$mail->IsHTML(true);
	
	//Check if email address exists and set reply address
	if($form['emailaddr'] != '')
	{
		$mail->AddReplyTo($form['emailaddr']);
	}
	
	// Add JA logo
	$mail->AddEmbeddedImage('../images/email_logo.png', 'logoimg', 'logo.png'); // attach file logo.jpg, and later link to it using identfier logoimg
	
    // 'To' Address
	$mail->AddAddress("joel@carciofino.com");
	
	$mail->Subject = "JAA Website - Enquiry";
			
	// Construct HTML only email body
	$mail->Body = "<p><img src=\"cid:logoimg\" />&nbsp;&nbsp;<font color=\"#666666\" size=\"5\" face=\"Arial, Helvetica, sans-serif\">Website Enquiry</font></p>
					<p><font color=\"#666666\" size=\"3\" face=\"Arial, Helvetica, sans-serif\">
					Name: ".$form['name']."<br />
					Email: ".$form['emailaddr']."<br />
					Phone Number: ".$form['contactnum']."<br /><br />
					".nl2br($form['enquiry'])."<br />
					</font></p>";
	
	// Construct Text only email body
	$mail->AltBody="Name: ".$form['name']."\n
					Email: ".$form['emailaddr']."\n
					Phone Number: ".$form['contactnum']."\n\n
					".$form['enquiry']."\n";
					
					
	if(!$mail->Send())
	{
		// Problem - echo email errors to screen
		echo "<p>Error processing email: " . $mail->ErrorInfo;
	}
	else
	{
		// Return to referring page with thank you
		unset($_SESSION[$sessionvar]);
		$_SESSION['thanks'] = true;
		header('Location: '.$referer['path']);
		exit();
	}
}
else
{
  //Redirect to home page.
  header('Location: http://www.johnalexanderadvisory.co.uk');
}

?>