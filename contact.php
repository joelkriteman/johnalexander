<?php
session_start();
#$_SESSION['ja_test2contact'] = 'a78c5bf69b40d464b954ef76815c6fa0';
#$_SESSION['ja_testcontact'] = 'a78c5bf69b40d464b954ef76815c6fa0';
$_SESSION['contact'] = 'a78c5bf69b40d464b954ef76815c6fa0';
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>John Alexander Advisory | Contact</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="x-ua-compatible" content="IE=9" />
<meta name="keywords" content="" />
<meta name="description" content="Contact John Alexander Advisory" />
<meta name="author" content="Lodge Information Services Limited | www.lisltd.co.uk" />
<meta name="copyright" content="Copyright <?php echo(date('Y'));?> " />
<meta name="robots" content="index, follow" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="revisit-after" content="7 Days" />
<meta name="format-detection" content="telephone=no" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>
<script language="Javascript" type="text/javascript" src="javascript/validate.js"></script>
<style>
#contact p
{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #FFFFCC;
}

</style>
</head>
<body>
<div id="container">
   <div id="bgwrap">
	<div id="bg">
	 <div id="maincontentwrap">
		<div id="logowrap">
		 <div id="logoleft">
		   <a href="index.php"><img src="images/logo.png" alt="John Alexander logo" border="0" title="John Alexander Advisory logo"></a>
		 </div><!-- end logoleft div -->
		 <div id="logocentre">
		  <h1>John Alexander Advisory</h1>
		  <h2>Independent Impartial Business Advisor</br>and Insolvency Practitioner</h2>		  
		 </div><!-- end logocentre div -->
		 <div id="logoright">
		  <img src="images/JohnAlexander.png" alt="Photo of John Alexander" border="0" title="Photo of John Alexander">
		 </div><!-- end logoright div -->
		</div><!-- end logowrap div -->
		<div id="nav">
		 <ul>
		  <li><a href="index.php">Home</a></li>
		  <li><a href="activities.php">Activities</a></li>
		  <li><a href="press.php">Press</a></li>
		  <li><a href="contact.php">Contact</a></li>
		 </ul>
		</div><!-- end of nav div -->
		<div id="contactformwrap">
		 <div id="contactformleft">
			 <form name="contactform" id="contactform" method="post" action="php-box/processform.php">
			  <p>Name</p>
			  <input id="name" name="name" type="text" />
			  <p>Email</p>
			  <input id="emailaddr" name="emailaddr" type="text" />
			  <p>Phone</p>
			  <input id="contactnum" name="contactnum" type="text" />
			  <p>Your Message</p>
			  <textarea name="enquiry" id="enquiry"></textarea>
			  <p style="padding-top: 30px;">
			    <input style="height: 50px; width: 150px; color: #666666; background-color: #FFFFCC; font-weight: bold;" type="button" name="button" value="Submit Details" onclick="validateContForm();" />
			  </p>			  
			 </form>
		 </div><!-- end contactformleft div -->
		 <div id="contactformright">
		  <p class="contactformrighthead">Get in touch</p>
		  <p class="contactformrightnorm">You can also contact us at:</p>
		  <p class="contactformrightbold"><a class="weblinks" href="mailto:john@johnalexanderadvisory.co.uk"><font color="#FF9900">john@johnalexanderadvisory.co.uk</font></a></p>
		  <p class="contactformrightbold">Tel: <font color="#FF9900">0788 411 2468</font></p>
		  <div id="contactformiconwrap">
		     <div id="contactformiconleft"><p class="contactformrightbold">Follow us&nbsp;</p></div>
			 <div id="contactformiconright"><p><a href="http://uk.linkedin.com/pub/john-alexander/4/8a1/235/" target="_blank"><img src="images/Linkedin_large.png" title="Follow John Alexander on LinkedIn" alt="Follow John Alexander on LinkedIn" border="0"></a>&nbsp;<a href="https://twitter.com/jag_alexander" target="_blank"><img src="images/Twitter_large.png" title="Follow John Alexander on Twitter" alt="Follow John Alexander on Twitter" border="0"></a></p></div>
		  </div>
		 </div><!-- end contactformright div -->
		</div><!-- end contactformwrap div -->
     </div><!-- end maincontentwrap div -->
     <div id="footerwrap">
	  <div id="footerleft">
	   <p>&copy; <?php echo(date('Y'));?> John Alexander (Advisory) Limited. All Rights Reserved</p>
	  </div><!-- end footer left div -->
	  <div id="footerright">
	   <p><a href="http://uk.linkedin.com/pub/john-alexander/4/8a1/235/" target="_blank"><img src="images/Linkedin.png" title="Follow John Alexander on LinkedIn" alt="Follow John Alexander on LinkedIn" border="0"></a>&nbsp;<a href="https://twitter.com/jag_alexander" target="_blank"><img src="images/Twitter.png" title="Follow John Alexander on Twitter" alt="Follow John Alexander on Twitter" border="0"></a></p>
	  </div><!-- end footer right div -->
     </div><!-- end footer wrap div -->  
	</div><!-- end bg div -->
   </div><!-- end bgwrap div -->   
   <div id="footerliswrap">
	<div id="footerlis">
	 <p>Designed by <a href="http://www.lisltd.co.uk">Lodge Information Services Ltd</a></p>
	</div><!-- end lis footer content --> 
   </div><!-- end lis footer wrap div -->   
</div><!-- end container div -->
<?php
if(isset($_SESSION['thanks']))
{
	?>
	<script language="javascript">
	$(window).load(function()
	{
		alert('                      Thank you\n\nYour message has been sent successfully!\n');
	});
	</script>
	<?php
	unset($_SESSION['thanks']);
	}
?>
</body>
</html>