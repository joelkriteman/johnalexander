<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>John Alexander Advisory | Activities</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="x-ua-compatible" content="IE=9" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="author" content="Lodge Information Services Limited | www.lisltd.co.uk" />
<meta name="copyright" content="Copyright <?php echo(date('Y'));?> " />
<meta name="robots" content="index, follow" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="revisit-after" content="7 Days" />
<meta name="format-detection" content="telephone=no" />
<link href="css/main.css" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" ></script>

<script type="text/javascript">
$(document).ready( function () {
  $('div#accordion h2').click( function () {
    $('div#accordion div.content').removeClass('open');
    $(this).next().addClass('open').slideDown('slow');
    $('div#accordion div.content:not(.open)').slideUp('fast');
  } );
} );
</script>

<style type="text/css">
div#accordion {
	width: 830px;
	clear: both;
}
div.content {
	display: none;
	color: #FFFFCC;
	text-align: left;
	padding-left: 15px;
}

div.content a {
   text-decoration: none;
   color: #FFFFCC;
}

#accordion h2 {
	cursor: pointer;
	color: #FFFFCC;
	text-align: left;

}
#accordion h3 {
	cursor: pointer;
	color: #FFFFCC;
	text-align: left;
	display:inline;
}

#maincontentupper {
	margin 0px;
	padding: 0px;
}
</style>

</head>
<body>
<div id="container">
   <div id="bgwrap">
	<div id="bg">
	 <div id="maincontentwrap">
		<div id="logowrap">
		 <div id="logoleft">
		   <a href="index.php"><img src="images/logo.png" alt="John Alexander logo" border="0" title="John Alexander Advisory logo"></a>
		 </div><!-- end logoleft div -->
		 <div id="logocentre">
		  <h1>John Alexander Advisory</h1>
		  <h2>Independent Impartial Business Advisor</br>and Insolvency Practitioner</h2>		  
		 </div><!-- end logocentre div -->
		 <div id="logoright">
		  <img src="images/JohnAlexander.png" alt="Photo of John Alexander" border="0" title="Photo of John Alexander">
		 </div><!-- end logoright div -->
		</div><!-- end logowrap div -->
		<div id="nav">
		 <ul>
		  <li><a href="index.php">Home</a></li>
		  <li><a href="activities.php">Activities</a></li>
		  <li><a href="press.php">Press</a></li>
		  <li><a href="contact.php">Contact</a></li>
		 </ul>
		</div><!-- end of nav div -->
		<div id="hpmaincontupper">
			<div id="accordion">
			  <h2>- <font color="#FF9900">Director of a Property Investment Company</font> <font size="-1">...click to read more</font></h2>
			  <div class="content">John Alexander was instructed by a lawyer who was acting in the matter of a private property investment company where an elderly managing director had become infirm. This had resulted in the company getting into difficulties with Companies House for late filing of accounts and HMRC for failing to submit tax returns and pay Corporation Tax and PAYE/NIC liabilities.  Shareholders were disgruntled as income had become erratic. In addition, the underlying properties had been allowed to slowly deteriorate over a number of years, threatening the sustainability of the business for the long term. John was appointed, initially as a Non-Executive Director and then as a Consultant to regularise the situation vis-&agrave;-vis Companies House and HMRC, which he achieved, and then to carry out a review of the underlying property holdings to ensure that the Company continues to be successful and once again provide its shareholders with a regular income.</div>
			  <h2>- <font color="#FF9900">Advisor regarding Company Acquisitions</font> <font size="-1">....click to read more</font></h2>
			  <div class="content">John Alexander, acting as an independent business advisor, has been engaged to assist in the acquisition of significant companies in the financial services field by senior, experienced individuals and major institutions all of whom have a successful track record in this area. </div>
			  <h2>- <font color="#FF9900">The Financial Reporting Council (FRC)</font> <font size="-1">...click to read more</font></h2>
			  <div class="content"><a href="https://www.frc.org.uk" target="_blank"><img style="padding-right:5px;" src="images/frc_logo.png" title="Financial Reporting Council logo" alt="Financial Reporting Council" border="0" align="left" /></a>John Alexander is on the FRC's Panel of Tribunal members who form the FRC Disciplinary Tribunals. The FRC sets the standards framework within which auditors, actuaries and accountants operate in the UK.  It also sponsors the UK Corporate Governance Code (for companies) and the Stewardship Code (for investors). It is the independent, investigative and disciplinary body for accountants and actuaries in the UK. The Conduct Committee of the FRC is responsible for operating and administering independent Disciplinary Schemes for accountants and actuaries. The Schemes provide that Executive Counsel shall conduct investigations in to matters where it appears to raise important issues affecting the public interest in the UK and there are reasonable grounds to suspect that there may have been misconduct. If appropriate, Executive Counsel will deliver a Formal Complaint to the Conduct Committee which will be considered by an independent FRC Disciplinary Tribunal.</div>
			  <h2>- <font color="#FF9900">ICAEW's Probate Committee</font> <font size="-1">...click to read more</font></h2>
			  <div class="content"><a href="https://www.icaew.com" target="_blank"><img style="padding-right:5px;" src="images/icaew_logo.png" title="ICAEW logo" alt="ICAEW - Institute of Chartered Accountants in England and Wales" border="0" align="left" /></a>John Alexander is on the newly created Committee set
			    up to establish and monitor the regulatory environment in which
			    appropriately qualified ICAEW Chartered Accountants will be able
			    to deliver probate
			    services as an alternative to traditional providers such as lawyers.
		    The Lord Chancellor has recently approved ICAEW's application to be a Regulator of probate services and a Licensing Authority for Alternative Business Structures (ABS). The Lord Chancellor has broken new ground in approving ICAEW's application as the first non-legal body to be able to regulate probate services and licence ABS's. This is part of the initiative of the Legal Services Board to transform the provision of legal services and give more choice to the consumer.</div>
			  <h2>- <font color="#FF9900">Cabinet Office's Non-Executive Board Member</font> <font size="-1">...click to read more</font></h2>
			  <div class="content">John Alexander has been accepted by the Cabinet Office on to its panel of individuals approved to be considered suitable to sit as a Non-Executive Member on Government Enhanced Departmental Boards.</br></br>
			    According to the Government's protocol: Government Departments are not the same as for-profit corporations, but they face many similar challenges. They need to be business-like. They can do this by tapping into the expertise of senior leaders with experience of managing complex organisations in the commercial private sector. These experts will provide challenge and support through their membership of Departmental Boards, which will provide the collective strategic and operational leadership of Government Departments. These Boards are advisory in the sense that they will provide advice to the department on issues within their remit, such as strategy and the deliverability of policies. They are supervisory in the sense that they scrutinise reporting from the department on performance, and challenge the department on how well it is achieving its objectives.
			  </div>
			  <h2>- <font color="#FF9900">Governor of Channing School</font> <font size="-1">...click to read more</font></h2>
			  <div class="content">Channing School is an independent day school for girls aged 4 - 18 at Highgate Hill in Highgate. Channing is a centre of academic excellence in North London, which ensures that every girl feels empowered, valued and supported. It provides a stimulating and vibrant educational experience that nurtures and sustains independent thinking, confidence and creativity. John Alexander is a Governor with particular responsibility for financial matters, is on the Finance and General Purposes Committee and Remuneration Committee and acts as a 'mentor' to the Bursar.</div>
			</div>
		</div><!-- end upper content div -->
     </div><!-- end maincontentwrap div -->
     <div id="footerwrap">
	  <div id="footerleft">
	   <p>&copy; <?php echo(date('Y'));?> John Alexander (Advisory) Limited. All Rights Reserved</p>
	  </div><!-- end footer left div -->
	  <div id="footerright">
	   <p><a href="http://uk.linkedin.com/pub/john-alexander/4/8a1/235/" target="_blank"><img src="images/Linkedin.png" title="Follow John Alexander on LinkedIn" alt="Follow John Alexander on LinkedIn" border="0"></a>&nbsp;<a href="https://twitter.com/jag_alexander" target="_blank"><img src="images/Twitter.png" title="Follow John Alexander on Twitter" alt="Follow John Alexander on Twitter" border="0"></a></p>
	  </div><!-- end footer right div -->
     </div><!-- end footer wrap div -->  
	</div><!-- end bg div -->
   </div><!-- end bgwrap div -->
   <div id="footerliswrap">
	<div id="footerlis">
	 <p>Designed by <a href="http://www.lisltd.co.uk">Lodge Information Services Ltd</a></p>
	</div><!-- end lis footer content --> 
   </div><!-- end lis footer wrap div -->
</div><!-- end container div -->
</body>
</html>