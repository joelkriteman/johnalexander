<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title>John Alexander Advisory | Press</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="x-ua-compatible" content="IE=9" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="author" content="Lodge Information Services Limited | www.lisltd.co.uk" />
<meta name="copyright" content="Copyright <?php echo(date('Y'));?> " />
<meta name="robots" content="index, follow" />
<meta name="distribution" content="global" />
<meta name="rating" content="General" />
<meta name="revisit-after" content="7 Days" />
<meta name="format-detection" content="telephone=no" />
<link href="css/main.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="container">
   <div id="bgwrap">
	<div id="bg">
	 <div id="maincontentwrap">
		<div id="logowrap">
		 <div id="logoleft">
		   <a href="index.php"><img src="images/logo.png" alt="John Alexander logo" border="0" title="John Alexander Advisory logo"></a>
		 </div><!-- end logoleft div -->
		 <div id="logocentre">
		  <h1>John Alexander Advisory</h1>
		  <h2>Independent Impartial Business Advisor</br>and Insolvency Practitioner</h2>		  
		 </div><!-- end logocentre div -->
		 <div id="logoright">
		  <img src="images/JohnAlexander.png" alt="Photo of John Alexander" border="0" title="Photo of John Alexander">
		 </div><!-- end logoright div -->
		</div><!-- end logowrap div -->
		<div id="nav">
		 <ul>
		  <li><a href="index.php">Home</a></li>
		  <li><a href="activities.php">Activities</a></li>
		  <li><a href="press.php">Press</a></li>
		  <li><a href="contact.php">Contact</a></li>
		 </ul>
		</div><!-- end of nav div -->
		<div id="presscontent">
		 <p><font color="#FF9900" size="3"><strong><a href="http://viewer.zmags.com/publication/85b8e039#/85b8e039/24" target="_blank"><em>&quot;Tales
		           from the frontline&quot;</em> -
         (Published in Economia)</a></strong></font> </p>
		 <p><strong><font color="#FF9900" size="3"><em>&quot;The economy
		         is recovering , but is all good news&quot; </em>- (Published by <a href="http://www.cbw.co.uk/business-be-wary-despite-positive-indicators/" target="_blank">CBW</a> and <a href="http://www.financialdirector.co.uk/financial-director/opinion/2329683/business-be-wary-despite-positive-indicators" target="_blank">Financial
		         Director</a>)</font></strong><br /> 
		         <a href="http://www.cbw.co.uk/business-be-wary-despite-positive-indicators/" target="_blank"><font color="#FFFFCC">Click here to read the
		   article published by CBW</font></a><br />
		   <a href="http://www.financialdirector.co.uk/financial-director/opinion/2329683/business-be-wary-despite-positive-indicators" target="_blank"><font color="#FFFFCC">Click here to read the article published
       by Financial Director</font></a></p>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
		 <p><font color="#FF9900" size="3"><strong><a href="http://www.insolvencynews.com/blog/post/150/how-to-scoop-up-a-failing-business-from-an-insolvency-practitioner" target="_blank"><em>&quot;How
		           to buy a business from an Insolvency Practitioner&quot;</em> -
		         (co-authored, published in Insolvency News)</a></strong></font></p>

		 <p><font color="#FF9900" size="3"><strong><a href="http://www.cbw.co.uk/pdf/CRI-Dec13.pdf" target="_blank"><em>&quot;It&#8217;s
		           all up for grabs!&quot;</em>Conflicting statistics -
		         (published by CBW)</a></strong></font></p>

		 <p><font color="#FF9900" size="3"><strong><em>&quot;2013 3rd Quarter Insolvency
		         Statistics&quot;</em> -
		         (Published in <a href="http://www.insolvencynews.com/blog/post/133/insolvency-service-q3-statistics-the-responses" target="_blank">Insolvency
		         News</a> and in <a href="http://www.ccrmagazine.com/index.php?option=com_content&task=view&id=10233&Itemid=37" target="_blank">CCRMagazine</a>)</strong></font><br />
		         <a href="http://www.insolvencynews.com/blog/post/133/insolvency-service-q3-statistics-the-responses" target="_blank"><font color="#FFFFCC">Click here to read the
		   article published by Insolvency News</font></a><br />
		   <a href="http://www.ccrmagazine.com/index.php?option=com_content&task=view&id=10233&Itemid=37" target="_blank"><font color="#FFFFCC">Click here to read the article published
       by CCRMagazine</font></a></p>

		 <p><font color="#FF9900" size="3"><strong><a href="http://www.financialdirector.co.uk/financial-director/opinion/2278224/surviving-the-recovery" target="_blank"><em>&quot;How
		           to Survive the Recovery&quot;</em> -
		         (Published in Financial Director)</a></strong></font>
		 </p>

		 <p><font color="#FF9900" size="3"><strong><a href="http://www.hrmagazine.co.uk/hro/features/1077434/what-happens-administrator-called" target="_blank"><em>&quot;What
		           happens when an Administrator comes in&quot;</em> -
         (Published in HR Magazine)</a></strong></font> </p>
		 
		 <p><font color="#FF9900" size="3"><strong><a href="https://twitter.com/jag_alexander/status/299488896016867328/photo/1" target="_blank"><em>&quot;High
		           Street Retailers struggle to survive&quot;</em> -
         (Comments in Accountancy Magazine)</a></strong></font> </p>
		 
		 <p><font color="#FF9900" size="3"><strong><a href="http://realbusiness.co.uk/article/10871-insolvencies-for-dummies" target="_blank"><em>&quot;Understanding
		           Insolvency terminology&quot;</em> -
         (Published in Real Business)</a></strong></font> </p>
		 
		 <p><font color="#FF9900" size="3"><strong><a href="http://www.insolvencynews.com/article/17318/industry/profile-john-alexander" target="_blank"><em>&quot;Fake
		           jewel heists, and rewriting the rulebooks&quot; </em>-
         (Published in Insolvency News)</a></strong></font> </p>
		</div><!-- end upper content div -->
     </div><!-- end maincontentwrap div -->
     <div id="footerwrap">
	  <div id="footerleft">
	   <p>&copy; <?php echo(date('Y'));?> John Alexander (Advisory) Limited. All Rights Reserved</p>
	  </div><!-- end footer left div -->
	  <div id="footerright">
	   <p><a href="http://uk.linkedin.com/pub/john-alexander/4/8a1/235/" target="_blank"><img src="images/Linkedin.png" title="Follow John Alexander on LinkedIn" alt="Follow John Alexander on LinkedIn" border="0"></a>&nbsp;<a href="https://twitter.com/jag_alexander" target="_blank"><img src="images/Twitter.png" title="Follow John Alexander on Twitter" alt="Follow John Alexander on Twitter" border="0"></a></p>
	  </div><!-- end footer right div -->
     </div><!-- end footer wrap div -->  
	</div><!-- end bg div -->
   </div><!-- end bgwrap div -->
   <div id="footerliswrap">
	<div id="footerlis">
	 <p>Designed by <a href="http://www.lisltd.co.uk">Lodge Information Services Ltd</a></p>
	</div><!-- end lis footer content --> 
   </div><!-- end lis footer wrap div -->
</div><!-- end container div -->
</body>
</html>